<script src="../js/get_prod_page.js"></script>
<div class="prod-item card" id=<?= $id ?>>
    <img class="prod-catalog-img card-img-top" src="../img/detail.png" alt="photo"/>
    <div class="card-body">
        <h5 class="card-title">
            <?= $prod_name ?>
        </h5>
        <p class="card-text"><?= $prod_cost ?> </p>
        <a class="prod-href btn btn-primary" href="/prod_page?prod_id=<?= $id ?>"> Подробнее</a>
    </div>
</div>