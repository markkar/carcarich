<?php

function template($view, $data)
{
    extract($data);

    ob_start();

    if (file_exists($view)) {
        require $view;
    } else {
        echo 'Template not found!';
    }

    return ob_get_clean();
}

function renderTemplate($view, $data)
{
    print(template($view, $data));
}

