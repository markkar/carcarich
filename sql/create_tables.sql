CREATE TABLE users
(
    id            INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    login         VARCHAR(30)                        NOT NULL,
    password      VARCHAR(40)                        NOT NULL,
    hash          VARCHAR(32) DEFAULT NULL,
    second_name   VARCHAR(30)                        NOT NULL,
    first_name    VARCHAR(30)                        NOT NULL,
    city          VARCHAR(40)                        NOT NULL,
    status        ENUM ('wholesale', 'retail')       NOT NULL,
    type          ENUM ('phis', 'jur')               NOT NULL,
    date_of_birth DATE                               NOT NULL,
    email         VARCHAR(40)                        NOT NULL,
    phone         VARCHAR(15)
);

CREATE TABLE get_points
(
    id     INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    region VARCHAR(40)                        NOT NULL,
    city   VARCHAR(40)                        NOT NULL,
    street VARCHAR(30)                        NOT NULL,
    number VARCHAR(5)                         NOT NULL
);

CREATE TABLE orders
(
    id          INTEGER PRIMARY KEY AUTO_INCREMENT      NOT NULL,
    user_id     INTEGER                                 NOT NULL,
    get_pointID INTEGER                                 NOT NULL,
    status      ENUM ('basket', 'in_proccess', 'ready') NOT NULL,
    order_date  DATE                                    NOT NULL,
    sum         INT                                     NOT NULL,

    FOREIGN KEY (user_id) REFERENCES clients (id) ON DELETE NO ACTION
);

CREATE TABLE entities
(
    id           INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name         VARCHAR(50)                        NOT NULL,
    legal_adress VARCHAR(60)                        NOT NULL,
    tax_number   VARCHAR(40)                        NOT NULL
);

CREATE TABLE entities_branches
(
    id        INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    entity_id INTEGER                            NOT NULL,
    region    VARCHAR(40)                        NOT NULL,
    city      VARCHAR(40)                        NOT NULL,
    street    VARCHAR(30)                        NOT NULL,
    number    VARCHAR(5)                         NOT NULL,

    FOREIGN KEY (entity_id) REFERENCES providers (id) ON DELETE CASCADE
);

CREATE TABLE car_types
(
    id        INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    brand     VARCHAR(30)                        NOT NULL,
    model     VARCHAR(40)                        NOT NULL,
    prod_year DATE                               NOT NULL
);

CREATE TABLE prods
(
    id          INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    car_type_id INTEGER     DEFAULT NULL,
    is_original BOOLEAN                            NOT NULL,
    photo       VARCHAR(40) DEFAULT NULL,
    category    VARCHAR(30)                        NOT NULL,
    description TEXT                               NOT NULL,
    FOREIGN KEY (car_type_id) REFERENCES car_types (id) ON DELETE NO ACTION
);

CREATE TABLE comments
(
    id      INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    prod_id INTEGER                            NOT NULL,
    text    TEXT                               NOT NULL,
    mark    DECIMAL(2, 1) CHECK ( mark > 0 AND mark <= 5),

    FOREIGN KEY (prod_id) REFERENCES prods (id) ON DELETE CASCADE
);

CREATE TABLE cars
(
    id           INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    number       VARCHAR(15)                        NOT NULL,
    client_id    INTEGER     DEFAULT NULL,
    car_type_id  INTEGER                            NOT NULL,
    engine_power DECIMAL(7, 2)                      NOT NULL,
    fuel_cons    DECIMAL(5, 2)                      NOT NULL,
    trunk_volume SMALLINT,
    color        VARCHAR(20)                        NOT NULL,
    vin          VARCHAR(17) DEFAULT NULL
);

CREATE TABLE prod_branch_order
(
    order_id  INTEGER NOT NULL,
    prod_id   INTEGER NOT NULL,
    branch_id INTEGER NOT NULL,
    PRIMARY KEY (order_id, prod_id, branch_id),
    count     INT     NOT NULL,

    FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE,
    FOREIGN KEY (prod_id) REFERENCES prods (id) ON DELETE CASCADE,
    FOREIGN KEY (branch_id) REFERENCES providers_branches (id) ON DELETE CASCADE
);

CREATE TABLE prod_entity
(
    prod_id   INTEGER NOT NULL,
    entity_id INTEGER NOT NULL,
    PRIMARY KEY (prod_id, entity_id),
    quantity  INT     NOT NULL,
    cost      INT     NOT NULL,

    FOREIGN KEY (prod_id) REFERENCES prods (id) ON DELETE CASCADE,
    FOREIGN KEY (entity_id) REFERENCES providers (id) ON DELETE CASCADE
);
