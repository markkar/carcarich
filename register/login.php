<?php
include_once '../config/config.php';
include_once 'check_user_type.php';

session_start();

// Функция для генерации случайной строки
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

// Соединямся с БД
$link = mysqli_connect($host, $user, $passwd);
mysqli_select_db($link, $dbname);

if (isset($_POST['submit'])) {
    // Вытаскиваем из БД запись, у которой логин равняеться введенному
    $query = mysqli_query($link, "SELECT password, login FROM users WHERE login='" . mysqli_real_escape_string($link, $_POST['login']) . "' LIMIT 1");
    $data = mysqli_fetch_assoc($query);

    // Сравниваем пароли
    if ($data['password'] === md5($_POST['password'])) {
        // Генерируем случайное число и хешируем его
        $hash = md5(generateCode(10));
        $login = $data['login'];


        // Записываем в БД новый хеш авторизации
        mysqli_query($link, "UPDATE users SET hash='$hash' WHERE login='$login';");

        // Ставим куки
        setcookie("login", $data['login'], time() + 60 * 60 * 24 * 30, "/");
        setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/", null, null, true);

        // Ставим сессию
        $_SESSION['user'] = [
            'id' => $data['id'],
            'login' => $data['login']
        ];

        header("Location: " . $url);
        exit();
    } else {
        print "Вы ввели неправильный логин/пароль";
    }
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>
<html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Вход</title>
    <!--    <link rel="stylesheet" type="text/css" href="register-style.css" />-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container py-4">
    <section id="content">
        <form method="POST" class="mx-auto" style="max-width: 700px;">
            <h1>Войти</h1>

            <div class="mb-3">
                <input class="form-control" type="text" placeholder="Username" name="login" required/>
            </div>
            <div class="mb-3">
                <input class="form-control" type="password" placeholder="Password" name="password" required/>
            </div>
            <div class="mb-3 mx-auto">
                <button class="btn btn-primary mr-2" type="submit" name="submit">
                    Авторизоваться
                </button>
                <a class="btn" href="register.php">Регистрация клиента</a>
                <a class="btn" href="register_provider.php">Регистрация поставщика</a>
            </div>
        </form><!-- form -->

    </section><!-- content -->
</div><!-- container -->
</body>
</html>
