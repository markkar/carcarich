<?php

include_once '../config/config.php';

session_start();

if (isset($_COOKIE['login']) && isset($_COOKIE['hash'])) {
    $link = mysqli_connect($host, $user, $passwd);
    mysqli_select_db($link, $dbname);

    $sql_set_hash_null = "UPDATE users SET hash = NULL WHERE login = " . $_COOKIE['login'] . ";";
    mysqli_query($link, $sql_set_hash_null);

    setcookie('login', null, -1, '/');
    setcookie('hash', null, -1, '/');

    session_destroy();

    header("Location: " . $url);
}




