<?php

function check_cookies()
{
    $url = 'http://localhost/';
    $host = 'mysql';
    $user = 'root';
    $passwd = 'mephiPassword';
    $dbname = 'autoshop_db';

    $link = mysqli_connect($host, $user, $passwd);
    mysqli_select_db($link, $dbname);

    if (isset($_COOKIE['login']) and isset($_COOKIE['hash'])) {
        $select_string = "SELECT * FROM users WHERE login = '" . $_COOKIE['login'] . "' LIMIT 1";
        $query = mysqli_query($link, $select_string);
        $user_data = mysqli_fetch_assoc($query);

        if ($user_data['hash'] === $_COOKIE['hash']) {
            return true;
        } else {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true);
            return false;
        }
    } else {
        return false;
    }
}
