<?php
// Страница регистрации нового пользователя

include_once '../config/config.php';
include_once './register_user.php';

// Соединямся с БД
$link = mysqli_connect($host, $user, $passwd);
mysqli_select_db($link, $dbname);

if (isset($_POST['submit'])) {

    $login = $_POST['login'];
    $password = $_POST['password'];

    $err = registerUser($login, $password);

    if (count($err) != 0) {
        print "<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach ($err as $error) {
            print "<p class='error'>" . $error . "</p>" . "<br>";
        }
        mysqli_close($link);
        exit();
    }

    $SQL_string_insert = "INSERT INTO clients (login, first_name, second_name, city, status, type, date_of_birth, email, phone) 
                               VALUES ('" . $login . "', '" . $_POST['first_name'] . "', '" . $_POST['second_name'] . "', '" .
        $_POST['city'] . "', '" . $_POST['status'] . "', '" . $_POST['type'] . "', '" . $_POST['date_of_birth'] . "', '" . $_POST['email'] . "', '" . $_POST['phone'] . "');";

    $res = mysqli_query($link, $SQL_string_insert);


    if (!$res) {
        print "<b>При регистрации произошлa ошибкa:</b><br>";
    } else {
        header("Location: login.php");
    }
}

mysqli_close($link);

?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>
<html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <!--    <link rel="stylesheet" type="text/css" href="register-style.css" />-->
</head>
<body>
<div class="container">
    <section id="content">
        <form method="POST" class="mx-auto" style="max-width: 700px;">
            <h1>Регистрация клиента</h1>
            <div class="mb-3">
                <input class="form-control" type="text" placeholder="Username" name="login" required/>
            </div>
            <div class="mb-3">
                <input class="form-control" type="password" placeholder="Password" name="password" required/>
            </div>

            <div class="mb-3">
                <input class="form-control" type="text" placeholder="Имя" required="" id="first_name"
                       name="first_name"/>
            </div>

            <div class="mb-3">
                <input class="form-control" type="text" placeholder="Фамилия" required="" id="second_name"
                       name="second_name"/>
            </div>

            <div class="mb-3">
                <input class="form-control" type="text" placeholder="City" required="" id="city" name="city"/>
            </div>

            <div class="mb-3">
                <input class="form-control" type="email" placeholder="Email" required="" id="email" name="email"/>
            </div>

            <div class="mb-3">
                <label for="date_of_birth" class="form-label">Дата рождения</label>
                <input class="form-control" type="date" placeholder="Date of birth" required="" id="date_of_birth"
                       name="date_of_birth">
            </div>

            <div class="mb-3">
                <input class="form-control" type="tel" placeholder="Phone number" required="" id="phone" name="phone">
            </div>


            <div>
                <p class="pt-3">Ваш статус:</p>
                <div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" value="wholesale"
                               id="flexRadioDefault1"
                               checked>
                        <label class="form-check-label" for="flexRadioDefault1">
                            Оптовый
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" value="retail"
                               id="flexRadioDefault2">
                        <label class="form-check-label" for="flexRadioDefault2">
                            Розница
                        </label>
                    </div
                </div>
            </div>

            <div>
                <p class="pt-3">Тип субъекта:</p>
                <div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="type" value="phis" id="phis" checked>
                        <label class="form-check-label" for="phis">
                            Физическое лицо
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="type" value="jur" id="jur">
                        <label class="form-check-label" for="jur">
                            Юридическое лицо
                        </label>
                    </div
                </div>
            </div>

            <div class="my-3">
                <button class="btn btn-primary mr-2" type="submit" name="submit">
                    Зарегистрироваться
                </button>
                <a class="btn" href="login.php">Авторизация</a>
            </div>

        </form><!-- form -->

    </section><!-- content -->
</div><!-- container -->
</body>
</html>