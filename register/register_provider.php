<?php
include_once '../config/config.php';
include_once 'register_user.php';

session_start();

$link = mysqli_connect($host, $user, $passwd);
mysqli_select_db($link, $dbname);

if (isset($_POST['submit'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];

    $err = registerUser($login, $password);

    if (count($err) != 0) {
        print "<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach ($err as $error) {
            print "<p class='error'>" . $error . "</p>" . "<br>";
        }
        mysqli_close($link);
        exit();
    }

    $company_name = $_POST['company_name'];
    $address = $_POST['address'];
    $inn = $_POST['inn'];
    $login = $_POST['login'];
    $password = md5($_POST['password']);

    $SQL_insert_provider = "INSERT INTO providers (name, legal_adress, tax_number, login) VALUES ('$company_name', '$address', '$inn', '$login');";
    $result = mysqli_query($link, $SQL_insert_provider);

    if ($result) {
        $id = mysqli_fetch_assoc(mysqli_query($link, "SELECT id FROM providers WHERE name = '$company_name'"))['id'];

        for ($i = 0; $i < 3; $i++) {
            if ($_POST["region_$i"] && $_POST["city_$i"] && $_POST["street_$i"] && $_POST["house_$i"]) {
                $region = $_POST["region_$i"];
                $city = $_POST["city_$i"];
                $street = $_POST["street_$i"];
                $house = $_POST["house_$i"];

                $SQL_insert_branch = "INSERT INTO providers_branches (entity_id, region, city, street, number) VALUES 
                ($id,'$region', '$city', '$city', '$house')";
                mysqli_query($link, $SQL_insert_branch);
            }
        }

        header("Location: login.php");

    } else {
        print "<b>При регистрации произошла ошибка</b><br>";
    }
}
mysqli_close($link);
?>

<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <!--    <link rel="stylesheet" type="text/css" href="register-style.css" />-->
</head>
<form method="POST">
    <h1>Регистрация поставщика</h1>
    <div>
        <input type="text" placeholder="Company name" name="company_name" required/>
    </div>

    <div>
        <input type="text" placeholder="Login" name="login" required/>
    </div>

    <div>
        <input type="password" placeholder="Password" name="password" required/>
    </div>

    <div>
        <input type="text" placeholder="Legacy address" name="address" required/>
    </div>

    <div>
        <input type="text" placeholder="ИНН" name="inn" required/>
    </div>

    <div>
        <div>
            <p>Филиал 1</p>
            <input type="text" placeholder="Region" name="region_1" required/>
            <input type="text" placeholder="City" name="city_1" required/>
            <input type="text" placeholder="Street" name="street_1" required/>
            <input type="text" placeholder="House" name="house_1" required/>
        </div>
        <div>
            <p>Филиал 2</p>
            <input type="text" placeholder="Region" name="region_2"/>
            <input type="text" placeholder="City" name="city_2"/>
            <input type="text" placeholder="Street" name="street_2"/>
            <input type="text" placeholder="House" name="house_2"/>
        </div>
        <div>
            <p>Филиал 3</p>
            <input type="text" placeholder="Region" name="region_3"/>
            <input type="text" placeholder="City" name="city_3"/>
            <input type="text" placeholder="Street" name="street_3"/>
            <input type="text" placeholder="House" name="house_3"/>
        </div>
    </div>

    <div>
        <input type="submit" value="Register" name="submit"/>
        <a href="login_provider.php">Авторизация</a>
        <a href="login.php">Авторизация покупателей</a>
    </div>

</form><!-- form -->