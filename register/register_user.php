<?php

session_start();

function registerUser($login, $password) {
    require '../config/config.php';


    $err = [];

    // проверям логин
    if (!preg_match("/^[a-zA-Z0-9]+$/", $login)) {
        $err[] = "Логин может состоять только из букв английского алфавита и цифр";
    }

    if (strlen($login) < 3 or strlen($login) > 30) {
        $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
    }

    if (count($err) !== 0) return $err;

    // Соединямся с БД
    $link = mysqli_connect($host, $user, $passwd);
    mysqli_select_db($link, $dbname);


    // проверяем, не сущестует ли пользователя с таким именем
    $users = mysqli_query($link, "SELECT login FROM users WHERE login = '" . mysqli_real_escape_string($link, $login) . "'");

    if (mysqli_num_rows($users) > 0) {
        $err[] = "Пользователь с таким логином уже существует в базе данных";
    }

    if (count($err) == 0) {
        $hash_password = md5(trim($password));

        $SQL_string_insert = "INSERT INTO users (login, password) VALUES ('$login', '$hash_password')";
        mysqli_query($link, $SQL_string_insert);
    }

    mysqli_close($link);

    return $err;
}