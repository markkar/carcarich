<!DOCTYPE html>

<?php
session_start();
include_once "register/check_cookies.php";
include_once "register/check_user_type.php";
?>

<html lang="ru">
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
</head>

<body>
<header class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">


        <a href="#" class="navbar-brand">CarCarich</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href='/catalog'>Каталог</a>
                </li>
                <?php
                if (check_cookies()) {
                    echo '<div class="nav-item"><a class="nav-link" href="">Моя корзина</a></div>';
                }
                ?>


            </ul>

            <?php
            $userType = checkUserType($_COOKIE['login']);
            if ($userType == 'provider') {
                echo "<span>Поставщик:</span>";
            }

            if ($userType == 'client') {
                echo "<span>Клиент:</span>";
            }
            if ($_COOKIE['login']) {
                echo '<div class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">' . $_COOKIE['login'] . '</a><ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="register/logout.php">Выйти</a></li>
          </ul></div>';
            }


            ?>
            <?php
            if (!check_cookies()) {
                echo '<a class="nav-link" href="register/login.php">Авторизация</a>';
            }
            ?>
        </div>
    </div>
</header>

<div class="container">
    <?= $content ?>
</div>

<div id="page-left"></div>
<div id="page-right"></div>

<footer>
    <div class="blank-footer-left"></div>
    <div class="footer"></div>
    <div class="blank-footer-right"></div>
</footer>


</html>
