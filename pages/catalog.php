<div class="catalog-page">
    <?php
    include_once "rendering.php";

    renderTemplate('templates/section_name.php', ['section_name' => 'Каталог']);

    echo "<br/>";

    ?>
    <div class="row">
        <?php
        for ($i = 0; $i < 12; $i++) {
            echo '<div class="col-6 col-sm-4 col-md-3 my-3">';
            renderTemplate('templates/product_item.php', ['prod_name' => 'Штука', 'prod_cost' => '7 руб', 'id' => $i + 1]);
            echo '</div>';
        }
        ?>
    </div>

</div>



