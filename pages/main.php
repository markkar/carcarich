<?php include_once "rendering.php"; ?>

<div class="main-page">
    <div id="site-greeting">
        <h1 class="main-page-title"><b>CarCarich</b> - магазин запчастей для иномарок</h1>
    </div>
    <div class="catalog-page">
        <div>
            <h2>Топ товаров по популярности</h2>
        </div>
        <div class="row">
            <?php
            for ($i = 0; $i < 6; $i++) {
                echo '<div class="col-6 col-sm-4 col-md-3 my-3">';
                renderTemplate('templates/product_item.php', ['prod_name' => 'Штука', 'prod_cost' => '7 руб', 'id' => '13']);
                echo '</div>';
            }
            ?>
        </div>

    </div>
</div>
