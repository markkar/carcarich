<?php
include_once "register/check_cookies.php";
include_once "rendering.php";
include_once "Router.php";

function show_main_page($page_content, $title) {
    renderTemplate('layout.php', ['content' => $page_content, 'title' => $title]);
}

Router::route('/', function () {
    show_main_page(template('pages/main.php', []), 'Главная страница');
});

Router::route('/catalog', function () {
    show_main_page(template('pages/catalog.php', []), 'Каталог');
});

Router::route('/prod_page\?prod_id=(\d+)', function ($prod_id) {
    show_main_page(template('pages/prod_page.php', ['id' => $prod_id]), 'Продукт');
});

Router::execute($_SERVER['REQUEST_URI']);


